<?php


require "../Database/DBModel.php";
use DBModel\DBModel;

    $testeme=DBModel::getInstance();
    $sql='CREATE TABLE IF NOT EXISTS ADMIN_TB(email VARCHAR(40) PRIMARY KEY)';
    $testeme->execute($sql);
    $sql2='CREATE TABLE IF NOT EXISTS POSTS_TB(   post_id serial  NOT NULL,
                                                  user_email varchar(40)  NULL,
                                                   user_fb_id bigint  NULL,
                                                   message text  NOT NULL,
                                                   link varchar(200)  NOT NULL,
                                                   time timestamp  NOT NULL,
                                                   status_code int  NULL,
                                                   CONSTRAINT POST_TB_pk PRIMARY KEY (post_id))';

    $testeme->execute($sql2);
