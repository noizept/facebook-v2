<?php
session_start();
require "../Facebook/Controller.php";
require "../Database/DBModel.php";
require "../Admin-Updates-Posts/tableHandler.php";
use Control_FB\Controller;
use DBModel\DBModel;


$controller=Controller::getInstance();
$session=$controller->getSession($_SESSION['fb-at']);
$dbmodel=DBModel::getInstance();
if(empty($session) || !$dbmodel->isAdmin($_SESSION['fb-email'])){
    header('Location:'. appLoginURL);
}

?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Social Post Scheduler</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">Post Scheduler</a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php echo $_SESSION["fb-fullname"] ?> <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="../Facebook/FBlogout.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li>
                        <a href="index.php"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                    </li>
                    <li>
                        <a href="post.php"><i class="fa fa-fw fa-edit"></i> Charts</a>
                    </li>
                    <li class="active">
                        <a href="admin_page.php"><i class="fa fa-fw fa-desktop"></i> Charts</a>
                    </li>

                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Admin <small>Page</small>
                        </h1>
                    </div>
                </div>
                <form action="#" method="post">
                    <div>
                    <input style="display: inline-block;" type="submit" name="filter" value="Pending" />
                    <input style="display: inline-block;" type="submit" name="filter" value="Rejected" />
                        <input style="display: inline-block;" type="submit" name="filter" value="Accepted" />
                        <div class="form-group">
                            <label style="display: inline-block;" >Email</label>
                            <input style="display: inline-block;" name="user_email" class="form-control">
                            <input style="display: inline-block;" type="submit" name="filter" value="Search" />

                        </div>

                    </div>

                </form>
               <!-- stuff goes here -->
                <?php fillTable($dbmodel);?>
            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="js/jquery.datetimepicker.css">
    <link rel="stylesheet" type="text/css" href="css/accordion.css">

    <script src="js/jquery.datetimepicker.js"></script>
    <script src="js/plugins/morris/raphael.min.js"></script>
    <script src="js/plugins/morris/morris.min.js"></script>
    <script src="js/plugins/morris/morris-data.js"></script>
    <script> jQuery('#datetimepicker').datetimepicker();</script>
</body>

</html>
