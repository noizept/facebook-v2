<?php
/**
 * Created by IntelliJ IDEA.
 * User: sergio
 * Date: 6/27/15
 * Time: 1:30 AM
 */
session_start();
$logout = $_SESSION["logout_url"];
session_destroy();
header('Location:'.$logout);