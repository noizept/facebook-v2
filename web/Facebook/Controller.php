<?php

/**
 * Created by IntelliJ IDEA.
 * User: sergio
 * Date: 6/26/15
 * Time: 11:18 AM
 */
namespace Control_FB;
require "../Global-Defines/fb-define.php";
require "../../vendor/autoload.php";

use Exception;
use Facebook\FacebookRequest;
use Facebook\FacebookRequestException;
use Facebook\FacebookSession;


class Controller
{

    /**
     * @var Stores the instance of this class
     */
    private static $mController;

    protected function __construct(){}

    /**
     * @return mixed
     * Returns the Instance SingleTon of this class
     */
    public static function getInstance()
    {
        if (!isset(static::$mController)) {
            static::$mController = new static;
        }
        return static::$mController;
    }

    /**
     * @param $aAccessToken - Access token from the Login URL
     * @return FacebookSession|string - Returns either the session or null String
     * Tries to Make a connection given a Access token and check if the token is valid
     */
    public  function getSession($aAccessToken)
    {
        FacebookSession::setDefaultApplication(appID, appSecret);
        $session = new FacebookSession($aAccessToken);
        try {
            $session->validate();
        } catch (FacebookRequestException $ex) {
            return '';
        } catch (\Exception $ex) {
            return '';
        }
        return $session;
    }

    /**
     * @param $session variable from Fsacebook
     * @param $message
     * @return bool returns sucess or not after the operation
     * Posts a  simple message in user timeline
     */
    public function postSelf($session,$message){
            $postRequest=new FacebookRequest($session,'POST','/me/feed',array('message'=>$message));
            try {
                $postRequest->execute();
            } catch (Exception $e) {
                return false;
            }
        return true;
    }

    /**
     * Gets all the pages with admin access that a user has
     * @param $session
     * @return array|null|void
     */
    public function getPages($session){
        $request = new FacebookRequest($session, 'GET', '/me/accounts');

        try{
           $response= $request->execute();
        }catch (Exception $e){
            return null;
        }

       return self::arrayToObjects($response->getGraphObject()->asArray());

    }
    /**
     * Converts an GraphObject Array into a Normal Array
     * @param $aArray graphObject array
     * @return array|void
     * Returns null if the array is empty
     * if not converts to a normal Array with id->pageid , access-token->page access token,name-> pagename
     */
    private function arrayToObjects($aArray){
        if(count($aArray['data'])<1) return;
        $simplifiedArray=[];
        foreach($aArray['data']  as  $arr){
            $pageinfo=[
                "id" =>$arr->id,
                "access_token" =>$arr->access_token,
                "name"=> $arr->name
            ];
            array_push($simplifiedArray,$pageinfo);
        }
        return $simplifiedArray;
    }

    /**
     * Returns the object with the Request Page information, Id,access_token,name
     * @param $session user session
     * @param $name Page name
     * @return mixed
     */
    public function getPageIdToken($session,$name){
        $pages= self::getPages($session);
        foreach($pages as $page){
            if($page["name"]==$name){
                return $page;
            }
        }

    }

    /**
     * Posts in a certain Page Timeline, if the user has admin rights only
     * @param $session , the user session
     * @param $pageId , the page Id to be posted on
     * @param $params , array with the information to post on the page
     * @throws FacebookRequestException
     */
    public function postPage($session,$pageId,$params){
        (new FacebookRequest($session,
            'POST',
            '/'.$pageId.'/feed',
            $params
            ))->execute();
    }


}