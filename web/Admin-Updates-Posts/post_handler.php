<?php
/**
 * Created by IntelliJ IDEA.
 * User: sergio
 * Date: 7/5/15
 * Time: 2:46 AM
 */
require "../Facebook/Controller.php";
require "../Database/DBModel.php";
use Control_FB\Controller;
use DBModel\DBModel;

/**
 * Validations if user is logged and is admin, and initiation of DBModel for db calls
 * and Controller for Facebook Calls
 */
        session_start();
        $controller=Controller::getInstance();
        $dbase=DBModel::getInstance();
        $session=$controller->getSession($_SESSION['fb-at']);
        if(empty($session) || !$dbase->isAdmin($_SESSION['fb-email'])){
            header('Location:'. appLoginURL);
        }

/**
 * Gets the previous action, from the Form to Acept or Refuse Post
 * And handles those actions
 */
        if($_POST["action"]=='Refuse'){
            $dbase->updatePost($_POST['post_id'],2);
            header('Location: ' . $_SERVER['HTTP_REFERER']);
        }
        if($_POST["action"]=='Accept') {
            $dbase->updatePost($_POST['post_id'], 1);
            $pageId = $controller->getPageIdToken($session,$_POST['page_name']);
            $params=[];
            $params['access_token']=$pageId['access_token'];
            $params['message'] = $_POST['message'];
            $params['link']=$_POST['link'];
            $dt = new DateTime();
            if(strtotime($_POST['time'])>strtotime($dt->format('Y-m-d H:i:s'))){
                $params['scheduled_publish_time']=strtotime($_POST['time']);
                $params['published']=false;
            }
            $controller->postPage($session,$pageId['id'],$params);
            header('Location: ' . $_SERVER['HTTP_REFERER']);
        }