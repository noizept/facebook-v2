<?php
/**
 * Created by IntelliJ IDEA.
 * User: sergio
 * Date: 7/4/15
 * Time: 5:16 AM
 */


    /**
     * This function will loop the array , and write its result into the Webpage
     * @param $dbmodel requires the singleton class from DBModel to get DB data
     */
     function fillTable($dbmodel)
     {
         $query = $dbmodel->getAllrows($_POST);

         while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
             ?>
                <div style='border-radius: 25px; border: 2px solid #8AC007;padding: 20px>'
                        <p> <b>   Post ID -> </b> <?php echo $row['post_id'];?> </p>
                        <p> <b>   User Email -> </b><?php echo   $row['user_email'];?> </p>
                        <p> <b>   User Facebook ID -> </b><?php echo  $row['user_fb_id'];?> </p>
                        <p> <b>   Message -> </b><?php echo  $row['message'];?> </p>
                        <p> <b>   Link -> </b><?php echo  $row['link'];?> </p>
                        <p> <b>   Schedule time -> </b><?php echo  $row['time'];?> </p>
                        <p> <b>   Status Code -> </b><?php echo status_code_to_string($row['status_code']);?> </p>
                        <p> <b>   Page Name -> </b><?php echo  $row['page_name'];?> </p>


                     <?php if(empty($row['status_code'])){?>
                     <form action="../Admin-Updates-Posts/post_handler.php" method="post">

                         <input type=hidden name="post_id" value="<?php echo $row['post_id'];?>">
                         <input type=hidden name="user_email" value="<?php echo $row['user_email'];?>">
                         <input type=hidden name="user_fb_id" value="<?php echo $row['user_fb_id'];?>">
                         <input type=hidden name="message" value="<?php echo $row['message'];?>">
                         <input type=hidden name="link" value="<?php echo $row['link'];?>">
                         <input type=hidden name="time" value="<?php echo $row['time'];?>">
                         <input type=hidden name="status_code" value="<?php echo $row['status_code'];?>">
                         <input type=hidden name="page_name" value="<?php echo $row['page_name'];?>">

                        <input style="display: inline-block;" type="submit" name="action" value="Accept" />
                        <input style="display: inline-block;"  type="submit" name="action" value="Refuse" />
                    </form>
                    <?php } ?>

                 </div>

            <?php
         }
     }

    function status_code_to_string($status_code){
        switch($status_code){
            case 0:
                return 'Pending';
            case 1:
                return 'Approved';
            case 2:
                return 'Rejected';
        }
    }