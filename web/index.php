<?php

//require "Global-Defines/fb-define.php";
header('P3P: CP="CAO PSA OUR"');
session_start();

require ("Global-Defines/fb-define.php");
require('../vendor/autoload.php');
use Facebook\FacebookRedirectLoginHelper;
use Facebook\FacebookRequest;
use Facebook\FacebookRequestException;
use Facebook\FacebookSession;


FacebookSession::setDefaultApplication(appID, appSecret);
$helper = new FacebookRedirectLoginHelper(appLoginURL); // login helper with redirect_uri
try
{
  $session = $helper->getSessionFromRedirect();
}
catch( FacebookRequestException $ex )
{
  echo $ex->getMessage();
}
catch( Exception $ex )
{
  echo $ex->getMessage();
  // When validation fails or other local issues
}


if ( isset( $session )){ // see if we have a session
    $_SESSION['fb-at'] = $session->getToken();
    $request = new FacebookRequest( $session, 'GET', '/me' ); // graph api request for user data

    $response = $request->execute();
    $graphObject = $response->getGraphObject(); // get response
    $_SESSION["fb-fullname"]=$graphObject->getProperty("name");
    $_SESSION['fb-email']=$graphObject->getProperty('email');
    $_SESSION['fb-id']=$graphObject->getProperty('id');

    $_SESSION["logout_url"]=$helper->getLogoutUrl($session,appLoginURL);
    header('Location: website/index.php');

   /*
    $graphObject_keys = array("id", "email", "first_name", "last_name", "gender", "link", "locale", "name", "timezone", "updated_time", "verified");
    for ($i=0; $i<count($graphObject_keys); $i++){
        echo $graphObject_keys[$i] . " : " . $graphObject->getProperty($graphObject_keys[$i]) . "<br/>";
    }
    echo '<a href="'.appLoginURL.'posts.php">Post to FB</a>';

   echo '<a href="' . $helper->getLogoutUrl($session,appLoginURL) . '">Logout</a>';
   */
} //end if
else {
        echo '<a href="' . $helper->getLoginUrl(array('scope' => 'email,publish_actions,manage_pages,publish_pages')) . '">Login</a>'; // show login url
} //end else
