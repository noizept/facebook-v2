<?php
/**
 * Created by IntelliJ IDEA.
 * User: sergio
 * Date: 7/3/15
 * Time: 5:46 PM
 */

/**
 * Validates and adds data to the database
 */

    session_start();
    require "../Facebook/Controller.php";
    require "DBModel.php";
    require"../AWS/AWService.php";

    use DBModel\DBModel;
    use Control_FB\Controller;

    // Verifies if user has a FB session, if he doesn't will be redirected to login Page
    $validator=Controller::getInstance();
    if(empty($validator->getSession($_SESSION['fb-at']))){
        header('Location:'. appLoginURL);
        exit;
    }

    //After validating the URL, checks if message  are empty, if so redirect to previous page
    if(empty($_POST["message"])) {
        header('Location: ' . $_SERVER['HTTP_REFERER']);
        exit;
    }

    //Verifies if there is a DateTime inserted, and if there is verifies if its date from the Past
    // in that case sets the schedule-date to current time
    $dt = new DateTime();
    $dt=$dt->format('Y-m-d H:i:s');
    if(empty($_POST['dateTime']) || strtotime($_POST['dateTime'])<=strtotime($dt) ) {
        $_POST['dateTime'] = $dt;
    }

    //Creates a AWService client, so we can upload the file coming from the form
    $AWSClient=new AWService();
    $fileLink=$AWSClient->upload($_FILES,$_SESSION['fb-id']);

    //Gets current Db Model, to insert data into the database
    $dbase=DBModel::getInstance();
    $success=$dbase->insertPost($_SESSION['fb-email'],$_SESSION['fb-id'],$_POST['message'],$fileLink,
        $_POST['dateTime'],0,$_POST['pageName']);

    //If the database insert is succeeded will display a message and redirect to previous page
    //if not, will display the error from the insertPost function called previous
        if($success){
            $url = $_SERVER['HTTP_REFERER'];
            echo "Added to Databased, need admin aproval";
            echo '<script type="text/javascript">
                        window.setTimeout(function(){
                        window.location.href = "'.$url.'";
                        }, 5000);
                </script>';
        }

