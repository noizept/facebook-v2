<?php
/**
 * Created by IntelliJ IDEA.
 * User: sergio
 * Date: 6/30/15
 * Time: 4:09 PM
 */



namespace DBModel;
use Exception;
use PDO;

require "../../vendor/autoload.php";
require "../Global-Defines/DBDefine.php";

class DBModel
{

    /**
     * @var mbDBConect
     * variable with the PDO object connection to invoke database CRUD operations
     */
    private static $mDBConect;
    /**
     * @var mbDBModel
     * variable for storing the singleton DBModel class responsible for calling CRUD
     * operations outside this class
     */
    private static $mDBModel;

    protected function __construct(){}

    /**
     * @return mbDBModel
     * Returns the Instance of this Singleton Class
     */
    public static function getInstance()
    {
        if (!isset(static::$mDBModel)) {
            static::$mDBModel = new static;
            self::startConnection();
        }
        return static::$mDBModel;
    }

    /**
     * Makes a connection to a Postgres Database, using the Global_Variables
     * @return null if it fails to connect
     */
    private  static function startConnection()
    {
        if (!isset(static::$mDBConect)) {
            try{
                static::$mDBConect= new PDO("pgsql:host=".DB_HOST.";
                port=".DB_PORT.";
                dbname=".DB_NAME.";
                user=".DB_USERNAME.";
                password=".DB_PASSWORD);
                self::$mDBConect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            }
            catch(\Exception $e){
                echo $e->getMessage();
                return null;
            }
        }
    }

    /**
     * @param $sql recieves a SQL paramenter and tries to execute the query against the Database
     * @return bool if it fails returns False
     */
    public function execute($sql){
        try {
            self::$mDBConect->exec($sql);
            echo "got here before excp";
        } catch(PDOException $e) {
            echo "got esception";
            echo $e->getMessage();
            return false;
        }
    }

    /**
     * @param $user_email - Facebook User Email
     * @param $user_fb_id - Facebook User Id
     * @param $message - The message coming from the form
     * @param $link - Link to be added on the message
     * @param $time - Time for the post to be scheduled
     * @param $status_code - 0 1 or 2 , Pending,Aproved,Refused
     * @param $page_name - The page name to be posted on
     * Recieves Posts paramenters and tries to insert into Posts Table, where all the posts
     * done by users are stored
     * @return bool , if success or not
     */
    public function insertPost($user_email,$user_fb_id,$message,$link,$time,$status_code,$page_name)
    {
        try {
            $statement = self::$mDBConect->prepare("INSERT INTO posts_tb(user_email,user_fb_id,message,link,time,status_code,page_name) VALUES(?, ?, ?, ?, ?, ?, ?)");
            $statement->execute(array($user_email, $user_fb_id, $message, $link, $time, $status_code, $page_name));
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
        return true;
    }

    /**
     * Returns true if the email is from an admin and false if not
     * @param $user_email
     * @return bool
     */
    public function isAdmin($user_email){
        $sql='SELECT COUNT(*) from admin_tb where email=?';
        $statement=self::$mDBConect->prepare($sql);
        $statement->execute(array($user_email));
        if($statement->fetchColumn()) {
            return true;
        }
        return false;
    }

    /**
     * Queries the database for all posts and returns the query result
     * @param $post
     * @return PDO -fetch
     */
    public function getAllrows($post){
        $sql='SELECT * FROM posts_tb';

        switch($post['filter']){
            case 'Pending':
                $sql.=' WHERE status_code=0';
                break;
            case 'Rejected':
                $sql.=' WHERE status_code=2';
                break;
            case 'Accepted':
                $sql.=' WHERE status_code=1';
                break;
            case 'Search':
                $escaped = pg_escape_string($post['user_email']);
                $sql.=" WHERE user_email='".$escaped."'";
                break;
            default:
                break;
        }
        $sthm=self::$mDBConect->prepare($sql);
        $sthm->execute();
        return $sthm;
    }

    /**
     * Given a PostID, will update it's status with the parament status code on the database
     * @param $postId
     * @param $status_code
     */
    public function updatePost($postId,$status_code){
        $sql="UPDATE posts_tb ";
        $sql.="SET status_code=".$status_code;
        $sql.="  WHERE post_id=".$postId;
        try {
            $sthm = self::$mDBConect->prepare($sql);
            $sthm->execute();
        }catch (Exception $e){
            echo $e->getMessage();
        }
    }
}
